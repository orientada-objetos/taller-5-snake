class Snake:
    def __init__ (self):
        self.color=(0,0,255)
        self.direction="RIGHT"
        self.body=[(2,0),(1,0),(0,0)]
    
    def updateCoordinates(self, x,y):
        if self.direction=="UP":
            y-=1
        elif self.direction=="DOWN":
            y+=1
        elif self.direction=="LEFT":
            x-=1
        elif self.direction=="RIGHT":
            x+=1
        return (x,y)
  
    def eat(self):
        for i in range(0,10):
            (x,y)=self.body[0] 
            x,y=self.updateCoordinates(x,y)
            self.body.insert(0,(x,y))
            
    def move(self):
        (x,y)=self.body[0]
        x,y=self.updateCoordinates(x,y)
        self.body.insert(0,(x,y))       
        self.body.pop()